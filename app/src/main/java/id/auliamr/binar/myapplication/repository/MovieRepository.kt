package id.auliamr.binar.myapplication.repository

import id.auliamr.binar.myapplication.Api.ApiService

class MovieRepository(private val apiService: ApiService) {
    suspend fun getMovie(key: String) = apiService.getPopularMovie(key)
    suspend fun getMovieById(movieId: Int, key: String) = apiService.getMovieById(movieId, key)
}