package id.auliamr.binar.myapplication.ui

import android.graphics.Color
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import id.auliamr.binar.myapplication.Database.HomeViewModel

@Composable
fun HomeScreen(navController: NavHostController, homeViewModel: HomeViewModel) {

    val context = LocalContext.current

    val email: String by HomeViewModel.getEmail().observeAsState("")
    HomeViewModel.getUsername(email)

    val movie by HomeViewModel.getMovie().observeAsState()

    Surface(color = Color.WHITE) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (txtUsername, txtHome, txtLogout, rvData) = createRefs()

            Text(
                text = "Welcome, " + HomeViewModel.username,
                fontSize = 16.sp,
                modifier = Modifier
                    .constrainAs(txtUsername) {
                        top.linkTo(parent.top, margin = 50.dp)
                        start.linkTo(parent.start, margin = 30.dp)
                    }
            )

            Text(
                text = "Logout",
                fontSize = 16.sp,
                modifier = Modifier
                    .constrainAs(txtLogout) {
                        top.linkTo(parent.top, margin = 50.dp)
                        end.linkTo(parent.end, margin = 30.dp)
                    }
                    .clickable {
                        HomeViewModel.clearDataStore()
                        navController.navigate("login") {
                            popUpTo("home") {
                                inclusive = true
                            }
                        }
                    }
            )

            Text(
                text = "Home",
                fontSize = MaterialTheme.typography.h5.fontSize,
                modifier = Modifier
                    .constrainAs(txtHome) {
                        top.linkTo(parent.top, margin = 88.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )
        }
    }
}