package id.auliamr.binar.myapplication.Database

import androidx.room.*
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import id.auliamr.binar.myapplication.Database.User

@Dao
interface UserDao {

    @Query("SELECT * FROM User WHERE email = :query")
    fun getUserByEmail(query: String): User

    @Query("SELECT username FROM User WHERE email = :query")
    fun getUsernameByEmail(query: String): String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun registerUser(user: User):Long

    @Query("SELECT * FROM User WHERE User.email = :email")
    fun getUserRegistered(email:String): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Delete
    fun deleteUser(user: User): Int

}