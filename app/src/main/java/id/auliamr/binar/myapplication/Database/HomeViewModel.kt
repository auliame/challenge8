package id.auliamr.binar.myapplication.Database

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import id.auliamr.binar.myapplication.repository.MovieRepository
import id.auliamr.binar.myapplication.repository.UserRepository
import id.auliamr.binar.myapplication.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val movieRepository: MovieRepository,
    private val loginDataStore: LoginDataStore
) : ViewModel() {

    var username: String? = ""

    fun clearDataStore() {
        viewModelScope.launch {
            loginDataStore.clearData()
        }
    }

    fun getEmail(): LiveData<String> {
        return loginDataStore.getEmail().asLiveData()
    }

    fun getUsername(email: String) {
        viewModelScope.launch {
            username = userRepository.getUsernameByEmail(email)
        }
    }

    fun getMovie() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepository.getMovie("471e33d18cca1c216a5735f21308be7d")))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }
}