package id.auliamr.binar.myapplication.repository

import android.content.Context
import id.auliamr.binar.myapplication.Database.LocalDatabase
import id.auliamr.binar.myapplication.Database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(context: Context) {

    private val mDb = LocalDatabase.getInstance(context)

    suspend fun getUserByEmail(email: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUserByEmail(email)
    }

    suspend fun getUsernameByEmail(email: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUsernameByEmail(email)
    }

    suspend fun insert(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.insertUser(user)
    }

}