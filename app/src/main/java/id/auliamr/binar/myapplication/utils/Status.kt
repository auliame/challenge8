package id.auliamr.binar.myapplication.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}